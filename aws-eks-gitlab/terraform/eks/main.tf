terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

variable "region" {
  default     = "us-east-1"
  description = "AWS region"
}

provider "aws" {
  region = var.region
  profile = "terraform"
}

data "aws_availability_zones" "available" {}

locals {
  cluster_name = "jesus-cristo"
}
