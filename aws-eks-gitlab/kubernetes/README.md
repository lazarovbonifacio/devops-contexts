# Passo a passo infra

# Sumário
1. Terraform
   1. Componentes de redes (VPC, Subnets, route tables, security groups, internet gateway)
   2. EKS
2. Kubernetes  
   1. Helm
      1. Prometheus
      2. ingress-nginx
   2. Aplicações
   3. Cert-manager
   4. Redis
3. DNS

# Construção

## Terraform
O arquivo de base para esse projeto pode ser encontrado [aqui](https://github.com/terraform-aws-modules/terraform-aws-eks/tree/v17.24.0/examples/complete), bem como demais configurações e especificações. Os comandos necessários para a implantação são:

1. `terraform init`
2. `terraform apply`

## Kubernetes

### Helm
Se houver interesse em monitoramento do cluster, sugiro instalar o prometheus. Segue instruções:

1. Prometheus

   Como guia para a instalação do prometheus você pode utilizar o seguinte conteúdo: https://www.youtube.com/watch?v=bwUECsVDbMA. Os arquivos de configurações foram baseados ness repositório: https://github.com/antonputra/tutorials/tree/main/lessons/072.

   ```bash
   # run first time
   helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
   helm repo update

   # run to install operator
   helm install [RELEASE_NAME] prometheus-community/kube-prometheus-stack
   --values prometheus-values.yaml \
   --version 16.10.0 \
   --namespace monitoring \
   --create-namespace

   # run to uninstall on current cluster
   helm uninstall [RELEASE_NAME]

   # run to upgrade on current cluster
   helm upgrade [RELEASE_NAME] prometheus-community/kube-prometheus-stack --install
   ```
   Acesse a documentação do prometheus em https://artifacthub.io/packages/helm/prometheus-community/kube-prometheus-stack.

Para que o cluster seja acessível externamente, precisamos configurar um ingress-nginx. Segue instruções:

2. Ingress-NGINX
   ```bash
   # run first time
   helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
   helm repo update

   # run to install on current cluster
   helm install [RELEASE_NAME] ingress-nginx/ingress-nginx

   # run to uninstall on current cluster
   helm uninstall [RELEASE_NAME]

   # run to upgrade on current cluster
   helm upgrade [RELEASE_NAME] [CHART] --install
   ```
   Acesse a documentação em https://artifacthub.io/packages/helm/ingress-nginx/ingress-nginx.

3. Cert-manager

   Antes de realizar a configuração do cert-manager, certifique-se de realizar as devidas configurações de DNS. Em seguida, siga os passos abaixo:

   ```bash
   helm repo add jetstack https://charts.jetstack.io
   helm repo update

   # run to install crds
   kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.12.0/cert-manager.crds.yaml

   # run to install in current cluster
   helm install \
   cert-manager jetstack/cert-manager \
   --namespace cert-manager \
   --create-namespace \
   --version v1.12.0 \
   # --set installCRDs=true
   ```

   Após esse passos basta configurar o ClusterIssuer e o Certificado no cluster que estão na pasta kubernetes. Configure também o ingress para os serviços, adicionando o campo 'tls'. Nele você deve indicar o nome do arquivo em que o recurso Certificado salva o conteúdo. Gere um recurso do tipo Certificado por namespace.

   Acesse a documentação em https://cert-manager.io/docs/installation/helm/. Há também um exemplo [aqui](https://cert-manager.io/docs/tutorials/acme/nginx-ingress/)
## Extra Helm

1. Redis

   O redis não possui um helm chart oficial. Desta forma, fiz um simples manifesto para implantá-lo em kubernetes. Consulte a pasta kubernetes e aplique o arquivo redis.yml.

   Caso haja interessa de HA, sugiro seguir essa [documentação](https://www.dragonflydb.io/guides/redis-kubernetes)

## DNS
Para todas as URLs que você registrar, crie um CNAME apontando para a URL do ELB na Amazon. Quando a requisição atingir o ingress do cluster ele vai saber para qual regra enviar baseado na URL. 