#!/bin/bash

env && buildah version && aws --version
CREDENTIALS_RESULT=$(aws sts assume-role --role-arn arn:aws:iam::<AW5-ACC0UN7>:role/ECR_imagesPush_Access --role-session-name $CI_PIPELINE_ID)
export AWS_SECRET_ACCESS_KEY=$(echo "$CREDENTIALS_RESULT" | jq .Credentials.SecretAccessKey | sed 's/"//g')
export AWS_ACCESS_KEY_ID=$(echo "$CREDENTIALS_RESULT" | jq .Credentials.AccessKeyId | sed 's/"//g')
export AWS_SESSION_TOKEN=$(echo "$CREDENTIALS_RESULT" | jq .Credentials.SessionToken | sed 's/"//g')
aws ecr get-login-password | buildah login --username AWS --password-stdin <AW5-ACC0UN7>.dkr.ecr.us-east-1.amazonaws.com

bash
