Após aplicar o arquivo cd-permission.yml eu executei os seguintes passos:

```
eksctl get iamidentitymapping --cluster jesus-cristo --region=us-east-1 --profile=dont_use
eksctl create iamidentitymapping --cluster jesus-cristo --region=us-east-1 --profile=dont_use --arn arn:aws:iam::<AW5-ACC0UN7>:role/EKS_KubernetesAPI_Access --username gitlab-ci --group gitlab-ci --no-duplicate-arns
eksctl get iamidentitymapping --cluster jesus-cristo --region=us-east-1 --profile=dont_use
```

Esse passo a passo está em conformidade com a seguinte documentação: https://docs.aws.amazon.com/pt_br/eks/latest/userguide/add-user-role.html